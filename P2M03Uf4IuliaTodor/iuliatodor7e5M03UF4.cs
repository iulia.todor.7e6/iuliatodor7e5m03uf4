﻿using System.Drawing;
using System.Net;
/// <summary>
/// Esta es la clase FiguraGeometrica, que tiene los datos de las figuras
/// </summary
abstract class FiguraGeometrica
{
    protected int codi;
    public int GetCodi
    {
        get { return this.codi; }
    }

    public int SetCodi
    {
        set { codi = value; }
    }
    protected string nom;
    public string GetNom
    {
        get { return this.nom; }
    }

    public string SetNom
    {
        set { nom = value; }
    }
    protected Color color { get; set; }

    public Color GetColor
    {
        get { return this.color; }
    }

    public Color SetColor
    {
        set { color = value; }
    }

    public abstract double Area();

    /// <summary>
    /// Valores por defecto de la Figura Geométrica
    /// </summary
    public FiguraGeometrica()
    {
        this.codi = 0;
        this.nom = "Defecto";
        this.color = Color.White;
    }

    public FiguraGeometrica(int codi, string nom, Color color)
    {
        this.codi = codi;
        this.nom = nom;
        this.color = color;
    }

    public FiguraGeometrica(FiguraGeometrica figura)
    {
        this.codi = figura.codi;
        this.nom = figura.nom;
        this.color = figura.color;

    }

    /// <summary>
    /// Método para imprimir los datos de la figura
    /// </summary
    public override string ToString() //override evita que esto clashee con la funcion ya existente toString
    {
        return "Esta figura tiene " + this.codi + " y nombre " + nom + " con color " + color;
    }
    /// <summary>
    /// Método para comprobar si dos objetos son iguales
    /// </summary
    public override bool Equals(object? obj)
    {
        FiguraGeometrica fg = (FiguraGeometrica)obj;
        return (codi == fg.codi);
    }
    /// <summary>
    /// Método para devolver el código de la figura
    /// </summary
    public override int GetHashCode()
    {
        return codi;
    }

}
/// <summary>
/// Clase para las figuras de tipo rectángulo
/// </summary
class Rectangle : FiguraGeometrica
{
    private double @base;
    public double GetBase
    {
        get { return this.@base; }
    }

    public double SetBase
    {
        set { @base = value; }
    }

    private double altura;

    public double GetAltura
    {
        get { return this.altura; }
    }

    public double SetAltura
    {
        set { altura = value; }
    }

    public Rectangle() : base()
    {
        this.@base = 1;
        this.altura = 2;
    }

    public Rectangle(double @base, double altura, int codi, string nom, Color color) : base(codi, nom, color)
    {
        this.@base = @base;
        this.altura = altura;
    }

    public Rectangle(Rectangle rectangle, int codi, string nom, Color color) : base(codi, nom, color)
    {
        this.@base = rectangle.@base;
        this.altura = rectangle.altura;
    }
    /// <summary>
    /// Método para obtener los datos de la figura
    /// </summary
    public override string ToString()
    {
        
        return "Este rectángulo tiene el código " + this.codi + " y nombre " + nom + " con color " + color + ". Tiene " + @base + " de base y " + altura + " de altura ";
    }

    /// <summary>
    /// Calcula el área del recátangulo
    /// </summary
    public override double Area()
    {
        return @base * altura;
    }
    /// <summary>
    /// Calcula el perímetro del rectángulo
    /// </summary
    public double Perimetre()
    {
        return (@base * 2) + (altura * 2);
    }

}
/// <summary>
/// Clase para las figuras de tipo triángulo
/// </summary
class Triangle : FiguraGeometrica
{
    public double @base;
    public double GetBase
    {
        get { return this.@base; }
    }

    public double SetBase
    {
        set { @base = value; }
    }

    public double altura;
    public double GetAltura
    {
        get { return this.altura; }
    }

    public double SetAltura
    {
        set { altura = value; }
    }


    public Triangle() : base()
    {
        this.@base = 2;
        this.altura = 3;
    }

    public Triangle(double @base, double altura, int codi, string nom, Color color) : base(codi, nom, color)
    {
        this.@base = @base;
        this.altura = altura;
    }

    public Triangle(Triangle triangle, int codi, string nom, Color color) : base(codi, nom, color)
    {
        this.@base = triangle.@base;
        this.altura = triangle.altura;

    }
    /// <summary>
    /// Metodo para obtener los datos de la figura
    /// </summary
    public override string ToString() //override evita que esto clashee con la funcion ya existente toString
    {
        return "Este triángulo tiene el código " + this.codi + " y nombre " + nom + " con color " + color + ". Tiene " + @base + " de base y " + altura + " de altura ";
    }
    /// <summary>
    /// Calcula el área del triángulo
    /// </summary
    public override double Area()
    {
        return (@base * altura) / 2;
    }
    /// <summary>
    /// Calcula el perímetro del triángulo asumiendo que este es isósceles
    /// </summary
    public double Perimetre()
    {
        double hipotenusa = Math.Sqrt(Math.Pow((@base / 2), 2) + Math.Pow(altura, 2));

        return Math.Round((@base + (hipotenusa * 2)), 2);
    }
}
/// <summary>
/// Clase para las figuras de tipo¡círculo
/// </summary
class Cercle : FiguraGeometrica
{
    private double radi;

    public double GetRadi
    {
        get { return radi; }
    }

    public double SetRadi
    {
        set { radi = value; }
    }


    public Cercle() : base()
    {
        this.radi = 1.5;
    }

    public Cercle(double radi, int codi, string nom, Color color) : base(codi, nom, color)
    {
        this.radi = radi;
    }

    public Cercle(Cercle cercle, int codi, string nom, Color color) : base(codi, nom, color)
    {
        this.radi = cercle.radi;
    }
    /// <summary>
    /// Método para obtener los datos de la figura
    /// </summary
    public override string ToString() //override evita que esto clashee con la funcion ya existente toString
    {
        return "Este círculo tiene el código " + this.codi + " y nombre " + nom + " con color " + color + ". Tiene " + radi + " de radio";
    }
    /// <summary>
    /// Método para calcular el área del círculo
    /// </summary
    public override double Area()
    {
        return Math.Round(Math.PI * Math.Pow(radi, 2), 2);
    }
    public double Perimetre()
    {
        return Math.Round((2 * Math.PI * radi), 2);
    }

}
/// <summary>
/// Prueba para los métodos de las figuras
/// </summary
internal class ProvaFigures
{
    private static void Main(string[] args)
    {
        //Declaramos estos dos rectángulos y comprobamos si son iguales.
        Rectangle rectangle = new Rectangle(2, 3, 1, "rectangulo", Color.Aqua);//Código 1
        Rectangle rectangle2 = new Rectangle(2, 3, 1, "rectangulo", Color.Aqua);//Código 1
        //Devuelve true
        Console.WriteLine("Son iguales? " + rectangle.Equals(rectangle2));
        Console.WriteLine(rectangle.GetHashCode());

        //Ahora el código es 3
        rectangle.SetCodi = 3;
        //Devuelve false
        Console.WriteLine("Son iguales? " + rectangle.Equals(rectangle2));
        Console.WriteLine(rectangle.GetHashCode());
        Console.WriteLine(rectangle.GetCodi);

        Triangle triangulo = new Triangle();
        
        Cercle circulo = new Cercle();

        Console.WriteLine(rectangle.ToString());
        Console.WriteLine("El área del rectángulo es: " + rectangle.Area());
        Console.WriteLine("El perímetro del rectángulo es: " + rectangle.Perimetre());
        Console.WriteLine();

        Console.WriteLine(triangulo.ToString());
        Console.WriteLine("El área del triangulo es: " + triangulo.Area());
        Console.WriteLine("El perímetro del triangulo es: " + triangulo.Perimetre());
        Console.WriteLine();

        Console.WriteLine(circulo.ToString());
        Console.WriteLine("El área del círculo es: " + circulo.Area());
        Console.WriteLine("El perímetro del círculo es: " + circulo.Perimetre());
    }
}